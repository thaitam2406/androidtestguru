package guru.android.tamhuynh.guru;

import org.junit.Test;

import java.util.ArrayList;

import guru.android.tamhuynh.guru.model.NewsDetail;

import static org.junit.Assert.assertEquals;

/**
 * Created by tamhuynh on 8/9/17.
 */

public class NewDetailTest {

    @Test
    public void checkValue(){
        NewsDetail newsDetail = new NewsDetail();

        newsDetail.setBy("TamHuynh");
        assertEquals("TamHuynh", newsDetail.getBy());

        newsDetail.setId(10);
        assertEquals(new Integer(10), newsDetail.getId());

        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(new Integer(1));
        newsDetail.setKids(integers);
        assertEquals(integers, newsDetail.getKids());

        newsDetail.setParent("20");
        assertEquals("20", newsDetail.getParent());

        newsDetail.setScore(100);
        assertEquals(new Integer(100), newsDetail.getScore());

        newsDetail.setTime(new Integer(123));
        assertEquals(new Integer(123), newsDetail.getTime());

        newsDetail.setText("comment");
        assertEquals("comment", newsDetail.getText());

        newsDetail.setTitle("title");
        assertEquals("title", newsDetail.getTitle());

        newsDetail.setType("story");
        assertEquals("story", newsDetail.getType());

        newsDetail.setUrl("google.com");
        assertEquals("google.com", newsDetail.getUrl());

    }
}
