package guru.android.tamhuynh.guru;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import guru.android.tamhuynh.guru.dagger.AppComponent;
import guru.android.tamhuynh.guru.dagger.AppModule;
import guru.android.tamhuynh.guru.dagger.AppModule_ProvideContextFactory;
import guru.android.tamhuynh.guru.dagger.DaggerAppComponent;
import guru.android.tamhuynh.guru.dagger.PresenterModule;
import guru.android.tamhuynh.guru.dagger.RestClientModule;
import guru.android.tamhuynh.guru.dagger.RestClientModule_ProvideMyGuruAPIFactory;
import guru.android.tamhuynh.guru.network.MyGURUAPI;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailNewsFragment;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailNewsView;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailsNewsPresenterImpl;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity_MembersInjector;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeScreenPresenterImpl;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeView;

import static org.mockito.Mockito.doNothing;
import static org.robolectric.RuntimeEnvironment.application;

/**
 * Created by TamHuynh on 9/9/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DaggerTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    HomeScreenPresenterImpl homeScreenPresenter;
    @Mock
    HomeView homeView;
    @Mock
    PresenterModule presenterModule;
    @Mock
    RestClientModule restClientModule;

    @Mock
    DetailsNewsPresenterImpl detailsNewsPresenter;
    @Mock
    DetailNewsView detailNewsView;

    @Mock
    HomeActivity homeActivity;
    @Mock
    DetailNewsFragment detailNewsFragment;

    @Mock
    MyGURUAPI myGURUAPI;
    AppComponent component;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        AppModule appModule = new AppModule(application);
        PresenterModule presenterModule = new PresenterModule();
        RestClientModule restClientModule = new RestClientModule();
        component = DaggerAppComponent.builder()
                .appModule(appModule)
                .restClientModule(restClientModule)
                .presenterModule(presenterModule)
                .build();
        component.inject(homeScreenPresenter);
        component.inject(detailsNewsPresenter);
        appModule.provideContext();
        restClientModule.provideMyGuruAPI();
        presenterModule.provideDetailNewsPresenter(appModule.provideContext().getApplicationContext());
        presenterModule.provideHomeScreenPresenter(appModule.provideContext().getApplicationContext());

        AppModule_ProvideContextFactory appModule_provideContextFactory = new AppModule_ProvideContextFactory(appModule);
        appModule_provideContextFactory.get();
        appModule_provideContextFactory.create(appModule);

        RestClientModule_ProvideMyGuruAPIFactory restClientModule_provideMyGuruAPIFactory
                = new RestClientModule_ProvideMyGuruAPIFactory(restClientModule);
        restClientModule_provideMyGuruAPIFactory.get();
        restClientModule_provideMyGuruAPIFactory.create(restClientModule);

    }

    @Test
    public void daggerAppComponent_AppModule_PresenterModule(){
        AppModule appModule = new AppModule(application);
        component = DaggerAppComponent.builder()
                .appModule(appModule)
                .presenterModule(new PresenterModule())
                .build();
    }

    @Test
    public void daggerAppComponent_AppModule_restClientModule(){
        AppModule appModule = new AppModule(application);
        component = DaggerAppComponent.builder()
                .appModule(appModule)
                .restClientModule(new RestClientModule())
                .build();
    }

    @Test
    public void testInjectHomeActivity_DetailFragment(){
        HomeActivity homeActivity = Robolectric.buildActivity(HomeActivity.class)
                .create()
                .start()
                .resume()
                .get();
        component.inject(homeActivity);
        detailNewsFragment = new DetailNewsFragment();
        component.inject(detailNewsFragment);
    }
    @Test
    public void testSomeThing(){
        doNothing().when(homeScreenPresenter).setHomeView(homeView);
        doNothing().when(homeScreenPresenter).getListNewIDs();
        doNothing().when(homeScreenPresenter).getListNewContent();

        doNothing().when(detailsNewsPresenter).setDetailView(detailNewsView);
        doNothing().when(detailsNewsPresenter).getListComment("123");
        doNothing().when(detailsNewsPresenter).resetData();


        MyApplication myApplication = new MyApplication();
        myApplication.onCreate();
        myApplication.getmAppComponent();
    }

    /*@Test
    public void testSomeThing_nullView(){

        doNothing().when(homeScreenPresenter).setHomeView(null);
        doNothing().when(homeScreenPresenter).getListNewIDs();
        doNothing().when(homeScreenPresenter).getListNewContent();

        doNothing().when(detailsNewsPresenter).setDetailView(null);
        doNothing().when(detailsNewsPresenter).getListComment("123");
        doNothing().when(detailsNewsPresenter).resetData();


        MyApplication myApplication = new MyApplication();
        myApplication.onCreate();
        myApplication.getmAppComponent();
    }*/


}
