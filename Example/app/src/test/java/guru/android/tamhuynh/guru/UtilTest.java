package guru.android.tamhuynh.guru;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import static org.junit.Assert.assertEquals;
/**
 * Created by TamHuynh on 9/8/17.
 */

public class UtilTest {
    /**
     09/08/2017 @ 2:31am (UTC)
     2017-09-08T02:31:10+00:00 in ISO 8601

     Fri, 08 Sep 2017 02:31:10 +0000 in RFC 822, 1036, 1123, 2822

     Friday, 08-Sep-17 02:31:10 UTC in RFC 2822

     2017-09-08T02:31:10+00:00 in RFC 3339
     */
    long currentTime = 1504837870;
    /**
     09/08/2017 @ 10:00am (UTC)
     2017-09-08T10:00:00+00:00 in ISO 8601

     Fri, 08 Sep 2017 10:00:00 +0000 in RFC 822, 1036, 1123, 2822

     Friday, 08-Sep-17 10:00:00 UTC in RFC 2822

     2017-09-08T10:00:00+00:00 in RFC 3339
     */
    long lastMinute =   1504837219;

    /**
     01/01/2016 @ 12:00pm (UTC)
     2016-01-01T12:00:00+00:00 in ISO 8601

     Fri, 01 Jan 2016 12:00:00 +0000 in RFC 822, 1036, 1123, 2822

     Friday, 01-Jan-16 12:00:00 UTC in RFC 2822

     2016-01-01T12:00:00+00:00 in RFC 3339
     */
    long lastYear =   1451649600;
    /**
     08/25/2017 @ 12:00am (UTC)
     2017-08-25T00:00:00+00:00 in ISO 8601

     Fri, 25 Aug 2017 00:00:00 +0000 in RFC 822, 1036, 1123, 2822

     Friday, 25-Aug-17 00:00:00 UTC in RFC 2822

     2017-08-25T00:00:00+00:00 in RFC 3339
     */
    long lastDay =   1503619200;
    /**
     *08/07/2017 @ 12:00pm (UTC)
     2017-08-07T12:00:00+00:00 in ISO 8601

     Mon, 07 Aug 2017 12:00:00 +0000 in RFC 822, 1036, 1123, 2822

     Monday, 07-Aug-17 12:00:00 UTC in RFC 2822

     2017-08-07T12:00:00+00:00 in RFC 3339
     */
    long lastHour =   1504785600;
    /**
     *06/01/2017 @ 12:00am (UTC)
     2017-06-01T00:00:00+00:00 in ISO 8601

     Thu, 01 Jun 2017 00:00:00 +0000 in RFC 822, 1036, 1123, 2822

     Thursday, 01-Jun-17 00:00:00 UTC in RFC 2822

     2017-06-01T00:00:00+00:00 in RFC 3339
     */
    long lastMonth =   1496275200;


    @Test
    public void checkGenerateTime_checkYears() throws  Exception {
        Util util = new Util();
        assertEquals("1 years ago", util.generateTime(lastYear,currentTime));
    }

    @Test
    public void checkGenerateTime_checkMonth() throws  Exception {
        Util util = new Util();
        assertEquals("3 months ago", util.generateTime(lastMonth,currentTime));
    }

    @Test
    public void checkGenerateTime_checkDate() throws  Exception {
        Util util = new Util();
        assertEquals("14 days ago", util.generateTime(lastDay,currentTime));
    }

    @Test
    public void checkGenerateTime_checkHour() throws  Exception {
        Util util = new Util();
        assertEquals("14 hour 31 minute 10 second ago", util.generateTime(lastHour,currentTime));
    }

    @Test
    public void checkGenerateTime_checkMinutes() throws  Exception {
        Util util = new Util();
        assertEquals("10 minute 51 second ago", util.generateTime(lastMinute,currentTime));
    }

    @Test
    public void getWebsiteName(){
        Util util = new Util();
        String website = "https://www.epochconverter.com/";
        assertEquals("www.epochconverter.com", util.getWebsiteName(website));
    }
    @Test
    public void getWebsiteName_Empty_returnEmpty(){
        Util util = new Util();
        String website = "";
        assertEquals("", util.getWebsiteName(website));
    }
    @Test
    public void getWebsiteName_NUll_returnEmpty(){
        Util util = new Util();
        String website = null;
        assertEquals("", util.getWebsiteName(website));
    }

    @Test
    public void getWebsiteName_No_Double_Flash(){
        Util util = new Util();
        String website = "https:www.epochconverter.com/";
        assertEquals("https:www.epochconverter.com", util.getWebsiteName(website));
    }

    @Test
    public void getWebsiteName_No_Flash(){
        Util util = new Util();
        String website = "https://www.epochconverter.com";
        assertEquals("www.epochconverter.com", util.getWebsiteName(website));
    }

    @Test
    public void getWebsiteName_NomalString(){
        Util util = new Util();
        String website = "abc";
        assertEquals("abc", util.getWebsiteName(website));
    }

    @Test
    public void schedules(){
        int threadCount = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
        Scheduler scheduler = Schedulers.from(threadPoolExecutor);
        Util util = new Util();
        Util.getScheduler();
//        assertEquals(scheduler, Util.getScheduler());
    }
}
