package guru.android.tamhuynh.guru;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.ui.adapter.NewListAdapter;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Created by TamHuynh on 9/10/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class NewListAdapterTest {

    private View mockView;
    private NewListAdapter newListAdapter;
    @Mock
    LayoutInflater mockInflater;
    @Mock
    ViewGroup mockParent;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockView = mock(View.class);

        List<NewsDetail> lstNew = new ArrayList<>();
        for(int i = 0; i < 5; i ++) {
            NewsDetail childComment = new NewsDetail();
            childComment.setId(11);
            childComment.setText("text");
            childComment.setBy("by");
            childComment.setTime(1234);
            lstNew.add(childComment);
        }
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();
        newListAdapter = new NewListAdapter(activity, lstNew);
    }

    @Test
    public void onCreateViewHolderTest(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        List<NewsDetail> lstNew = new ArrayList<>();
        for(int i = 0; i < 5; i ++) {
            NewsDetail childComment = new NewsDetail();
            childComment.setId(11);
            childComment.setText("text");
            childComment.setBy("by");
            childComment.setTime(1234);
            lstNew.add(childComment);
        }



        TestableCandiesListAdapter testableAdapter = new TestableCandiesListAdapter(activity,lstNew);
        testableAdapter.setMockView(mockView);
        NewListAdapter.ViewHolderNewsList viewHolderNewsList =
                testableAdapter.onCreateViewHolder(new FrameLayout(RuntimeEnvironment.application), 0);
        assertEquals(viewHolderNewsList.itemView,mockView);

//        viewHolderNewsList.onClick(new FrameLayout(RuntimeEnvironment.application));

//        View view = mockInflater.inflate(R.layout.item_new_home,null);
//        assertEquals(view,newListAdapter.getLayout(new FrameLayout(RuntimeEnvironment.application)));
        Assert.assertNotNull("Response cant be null",newListAdapter.getLayout(new FrameLayout(RuntimeEnvironment.application)));

        /*ViewGroup viewGroup = new FrameLayout(RuntimeEnvironment.application);
        LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_new_home,viewGroup);
        assertEquals(view,newListAdapter.getLayout(viewGroup));*/

    }

    //Here we subclass and override the test subject again so we can use a mock view for testing, instead of the real one.
    static class TestableCandiesListAdapter extends NewListAdapter {
        public View mockView;

        public TestableCandiesListAdapter(Context context, List<NewsDetail> newsList) {
            super(context, newsList);
        }

        public void setMockView(View mockView) {
            this.mockView = mockView;
        }

        @Override
        public View getLayout(ViewGroup parent) {
            return mockView;
        }
    }

    @Test
    public void itemCount() {

        assertEquals(newListAdapter.getItemCount(),5);
    }

    @Test
    public void itemCount_newsListNULL() {
        newListAdapter.newsList = null;
        assertEquals(newListAdapter.getItemCount(),0);
    }

    @Test
    public void addNewsList_newsListNULL(){
        newListAdapter.newsList = null;
        List<NewsDetail> lstNew = new ArrayList<>();
        for(int i = 0; i < 5; i ++) {
            NewsDetail childComment = new NewsDetail();
            childComment.setId(11);
            childComment.setText("text");
            childComment.setBy("by");
            childComment.setTime(1234);
            lstNew.add(childComment);
        }
        newListAdapter.addNewsList(lstNew);
    }

    @Test
    public void addNewsList_newsListNotNULL(){
        List<NewsDetail> lstNew = new ArrayList<>();
        for(int i = 0; i < 5; i ++) {
            NewsDetail childComment = new NewsDetail();
            childComment.setId(11);
            childComment.setText("text");
            childComment.setBy("by");
            childComment.setTime(1234);
            lstNew.add(childComment);
        }
        newListAdapter.addNewsList(lstNew);
    }

    @Test
    public void getItemIdTest(){
        assertEquals(0,newListAdapter.getItemId(0));
    }
    private NewListAdapter.ViewHolderNewsList holder;
    @Test
    public void onBindViewHolderTest(){
        List<NewsDetail> lstNew = new ArrayList<>();
        List<Integer> kids = new ArrayList<>();
        kids.add(new Integer(10));
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setTitle("title");
        childComment.setScore(10);
        childComment.setBy("by");
        childComment.setUrl("https://www.google.com.sg");
        childComment.setTime(1234);
        childComment.setKids(kids);
        lstNew.add(childComment);
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();
        newListAdapter = new NewListAdapter(activity, lstNew);



        LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //We have a layout especially for the items in our recycler view. We will see it in a moment.
        View listItemView = inflater.inflate(R.layout.item_new_home, null, false);
        holder = new NewListAdapter.ViewHolderNewsList(listItemView);
        newListAdapter.bindViewHolder(holder,0);
    }

    @Test
    public void onBindViewHolderTest_DataNUll(){
        List<NewsDetail> lstNew = new ArrayList<>();
        List<Integer> kids = new ArrayList<>();
        kids.add(new Integer(10));
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText(null);
        childComment.setTitle(null);
        childComment.setScore(null);
        childComment.setBy(null);
        childComment.setUrl(null);
        childComment.setTime(null);
        childComment.setKids(null);
        lstNew.add(childComment);
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();
        newListAdapter = new NewListAdapter(activity, lstNew);



        LayoutInflater inflater = (LayoutInflater) RuntimeEnvironment.application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //We have a layout especially for the items in our recycler view. We will see it in a moment.
        View listItemView = inflater.inflate(R.layout.item_new_home, null, false);
        holder = new NewListAdapter.ViewHolderNewsList(listItemView);
        newListAdapter.bindViewHolder(holder,0);
    }
}
