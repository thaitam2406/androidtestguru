package guru.android.tamhuynh.guru;

import android.os.Build;
import android.support.annotation.RequiresApi;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import butterknife.Unbinder;
import guru.android.tamhuynh.guru.eventbus.ClickNewsEvent;
import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity;

/**
 * Created by TamHuynh on 9/9/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class HomeActivityTest {
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Test
    public void testSomething_backpresswithNoFragment(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();
        activity.onBackPressed();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        ClickNewsEvent clickNewsEvent = new ClickNewsEvent(newsDetail);
        activity.onEventMainThread(clickNewsEvent);


    }

    @Test
    public void testSomething_backPressedWithHasFragment(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        ClickNewsEvent clickNewsEvent = new ClickNewsEvent(newsDetail);
        activity.onEventMainThread(clickNewsEvent);
        activity.onBackPressed();

    }

    @Test
    public void addDataNewsIntoUI_noData(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        activity.addDataNewsIntoUI(null);
    }

    @Test
    public void addDataNewsIntoUI_hasData(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        List<NewsDetail> newsDetails = new ArrayList<>();
        newsDetails.add(newsDetail);
        activity.addDataNewsIntoUI(newsDetails);

//        activity.addFragmentComment(newsDetail);
    }

    @Test
    public void setUpPullToRefresh_sizeZero(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        List<NewsDetail> newsDetails = new ArrayList<>();
        newsDetails.add(newsDetail);
        activity.addDataNewsIntoUI(newsDetails);

        activity.refreshLayoutHome.setRefreshing(true);
    }

    @Test
    public void setUpPullToRefresh_sizeMoreThanZero(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        List<NewsDetail> newsDetails = new ArrayList<>();
        newsDetails.add(newsDetail);
        activity.addDataNewsIntoUI(null);

        activity.handleOnRefreshData();
    }

    @Test
    public void setUpRecyclerView_sizeZero(){
        final HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();
        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        List<NewsDetail> newsDetails = new ArrayList<>();
        for(int i = 0; i < 20; i ++)
            newsDetails.add(newsDetail);
        activity.addDataNewsIntoUI(newsDetails);
        activity.handleOnRefreshData();
    }

    @Test
    public void handleOnLoadMoreRequest_SizeDataZero(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .stop()
                .destroy()
                .get();
        List<NewsDetail> listCommentReply = new ArrayList<>();
        activity.setNewsDetails(listCommentReply);
        activity.handleOnLoadMoreRequest();
    }

    @Test
    public void handleOnLoadMoreRequest_SizeDataMoreThanZero(){
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .stop()
                .destroy()
                .get();
        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);
        activity.setNewsDetails(listCommentReply);
        activity.handleOnLoadMoreRequest();
    }

}
