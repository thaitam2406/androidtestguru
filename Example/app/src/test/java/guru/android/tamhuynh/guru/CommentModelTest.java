package guru.android.tamhuynh.guru;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import guru.android.tamhuynh.guru.model.CommentModel;
import guru.android.tamhuynh.guru.model.NewsDetail;

import static org.junit.Assert.assertEquals;

/**
 * Created by tamhuynh on 8/9/17.
 */

public class CommentModelTest {

    @Test
    public void testModel(){
        CommentModel commentModel = new CommentModel();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        commentModel.setMainComment(newsDetail);
        assertEquals(newsDetail, commentModel.getMainComment());

        List<NewsDetail> newsDetailList = new ArrayList<>();
        newsDetailList.add(newsDetail);
        commentModel.setListCommentReply(newsDetailList);
        assertEquals(newsDetailList, commentModel.getListCommentReply());
    }
}
