package guru.android.tamhuynh.guru;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import guru.android.tamhuynh.guru.eventbus.ClickNewsEvent;
import guru.android.tamhuynh.guru.model.NewsDetail;

/**
 * Created by tamhuynh on 8/9/17.
 */
public class ClickNewsEventTest {

    @Test
    public void checkEventBus(){
        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        ClickNewsEvent clickNewsEvent = new ClickNewsEvent(newsDetail);
        Assert.assertEquals(newsDetail, clickNewsEvent.getNewsDetail());

        NewsDetail newsDetail1 = new NewsDetail();
        newsDetail.setId(20);
        clickNewsEvent.setNewsDetail(newsDetail1);
        Assert.assertEquals(newsDetail1, clickNewsEvent.getNewsDetail());
    }
}
