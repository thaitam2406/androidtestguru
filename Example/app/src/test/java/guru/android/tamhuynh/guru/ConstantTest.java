package guru.android.tamhuynh.guru;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by tamhuynh on 8/9/17.
 */

public class ConstantTest {

    @Test
    public void checkConstant(){
        Constant constant = new Constant();
        assertEquals(true,constant.IS_DEVLOPMENT);
        assertEquals("https://hacker-news.firebaseio.com/v0/",constant.BASE_URL);
        assertEquals(25,constant.PAGING);

    }
}
