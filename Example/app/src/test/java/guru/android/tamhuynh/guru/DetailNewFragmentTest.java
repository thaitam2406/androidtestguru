package guru.android.tamhuynh.guru;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import java.util.ArrayList;
import java.util.List;

import guru.android.tamhuynh.guru.dagger.AppComponent;
import guru.android.tamhuynh.guru.dagger.AppModule;
import guru.android.tamhuynh.guru.dagger.DaggerAppComponent;
import guru.android.tamhuynh.guru.model.CommentModel;
import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailNewsFragment;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailsNewsPresenterImpl;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeScreenPresenterImpl;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeView;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertNotNull;
import static org.robolectric.RuntimeEnvironment.application;
import static org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;

/**
 * Created by TamHuynh on 9/10/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DetailNewFragmentTest {

    @Mock
    DetailNewsFragment fragment;
    @Mock
    HomeView homeView;
    @Mock
    HomeScreenPresenterImpl homeScreenPresenter;
    @Mock
    DetailsNewsPresenterImpl detailsNewsPresenter;
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        MyApplication myApplication = new MyApplication();
        myApplication.onCreate();
        AppModule appModule = new AppModule(myApplication);
        AppComponent component = DaggerAppComponent.builder()
                .appModule(appModule)
                .build();
        component.inject(homeScreenPresenter);
        component.inject(detailsNewsPresenter);
        detailsNewsPresenter = new DetailsNewsPresenterImpl(appModule.provideContext());
        homeScreenPresenter = new HomeScreenPresenterImpl(appModule.provideContext());
    }

    @Test
    public void testFragment_hasBundle(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",newsDetail);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
        fragment.onDetach();
        fragment.onDestroy();
        fragment.onDestroyView();
        assertNotNull( fragment.getTextView() );

//        activity.onCreate(null,null);

        /*Button button = (Button) activity.findViewById(R.id.hide);
        button.performClick();

        assertThat(button.getVisibility(), is(View.GONE));*/
    }

    @Test
    public void testFragment_noBundle(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",null);
        fragment.setArguments(null);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
    }

    @Test
    public void testFragment_hasBundleDataInvalid(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(10);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",newsDetail);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
    }

    @Test
    public void testFragment_hasBundleDataNull(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",newsDetail);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
    }

    @Test
    public void addCommentIntoUI_dataNull(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        NewsDetail newsDetail = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",newsDetail);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );

        fragment.addCommentIntoUI(null);
    }

    @Test
    public void addCommentIntoUI_hasData(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);
        mainComment.setText("text");
        mainComment.setBy("by");
        mainComment.setTime(1234);
        commentModel.setMainComment(mainComment);

        List<NewsDetail> list = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        list.add(childComment);
        commentModel.setListCommentReply(list);
        commentModels.add(commentModel);

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
        fragment.addCommentIntoUI(commentModels);
    }

    @Test
    public void addCommentIntoUI_hasDataFieldNULL(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);
        mainComment.setText("text");
        mainComment.setBy("by");
        mainComment.setTime(1234);
        commentModel.setMainComment(mainComment);

        List<NewsDetail> list = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
//        childComment.setText("text");
//        childComment.setBy("by");
//        childComment.setTime(1234);
        list.add(childComment);
        commentModel.setListCommentReply(list);
        commentModels.add(commentModel);

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
        fragment.addCommentIntoUI(commentModels);
    }

    @Test
    public void addCommentIntoUI_mainCommentNull(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();

        List<NewsDetail> list = null;
        commentModel.setListCommentReply(list);

        commentModels.add(commentModel);

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
        fragment.addCommentIntoUI(commentModels);
    }

    @Test
    public void addCommentIntoUI_CommentModelItemNUll(){
        DetailNewsFragment fragment = new DetailNewsFragment();
        HomeActivity activity = Robolectric.buildActivity( HomeActivity.class )
                .create()
                .start()
                .resume()
                .get();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add( fragment, null );
        fragmentTransaction.commit();

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();

        List<NewsDetail> list = null;
        commentModel.setListCommentReply(list);

        commentModels.add(null);

        SupportFragmentTestUtil.startVisibleFragment(fragment);
        startFragment( fragment );
        assertNotNull( fragment );
        fragment.addCommentIntoUI(commentModels);
    }

    @Test
    public void homeScreenPresenter_totalMoreThanCurrent(){
        homeScreenPresenter.setHomeView(homeView);
        homeScreenPresenter.currentPosition = 1;
        homeScreenPresenter.total = 2;
        homeScreenPresenter.listNewsID = new ArrayList<>();
        for(int i = 0; i < 40; i ++) {
            homeScreenPresenter.listNewsID.add("1");
        }

        homeScreenPresenter.getListNewContent();
    }

    @Test
    public void homeScreenPresenter_totalMoreThanCurrent1(){
        homeScreenPresenter.setHomeView(homeView);
        homeScreenPresenter.currentPosition = 1;
        homeScreenPresenter.total = 2;
        homeScreenPresenter.listNewsID = new ArrayList<>();
        for(int i = 0; i < 20; i ++) {
            homeScreenPresenter.listNewsID.add("1");
        }

        homeScreenPresenter.getListNewContent();
    }

    @Test
    public void homeScreenPresenter_totalMoreThanCurrent2(){
        homeScreenPresenter.setHomeView(homeView);
        homeScreenPresenter.currentPosition = 1;
        homeScreenPresenter.total = 2;
        homeScreenPresenter.listNewsID = new ArrayList<>();
        for(int i = 0; i < 20; i ++) {
            if(i == 0)
                homeScreenPresenter.listNewsID.add("1");
            else if(i == 1)
                homeScreenPresenter.listNewsID.add(null);
            else
                homeScreenPresenter.listNewsID.add("");
        }

        homeScreenPresenter.getListNewContent();
    }

    @Test
    public void homeScreenPresenter_totalLessThanCurrent(){
        homeScreenPresenter.setHomeView(homeView);
        homeScreenPresenter.currentPosition = 1;
        homeScreenPresenter.total = 1;
        homeScreenPresenter.listNewsID = new ArrayList<>();
        for(int i = 0; i < 40; i ++) {
            homeScreenPresenter.listNewsID.add("1");
        }

        homeScreenPresenter.getListNewContent();
    }

    @Test
    public void homeScreenPresenter_HomeViewNullTest(){
        homeScreenPresenter.getListNewContent();
    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_hasMainComment(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
//        childComment.setText("text");
//        childComment.setBy("by");
//        childComment.setTime(1234);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("13");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_commentModelsNUll(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);
        detailsNewsPresenter.commentModels = null;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_MainComment_sizeZero(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(listCommentReply);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(11);
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }


    @Test
    public void detailsNewsPresenterTest_addNewsDetail_MainCommentNUll(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
//        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(listCommentReply);

        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_commentModelItemNULL(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
//        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(listCommentReply);

        commentModels.add(null);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_hasMainCommentMatchNewDetail(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("10");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_hasChildCommentMatchNewDetail(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("11");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_ChildListCommentNUll(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(null);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("15");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_ChildListCommentNUll_matchWithMainComment(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);

        commentModel.setListCommentReply(null);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("10");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_ChildItemCommentNUll(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(null);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("20");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_ChildItemMatchParent(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("11");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void detailsNewsPresenterTest_addNewsDetail_ChildItemNotMatchParent(){
        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);

        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);

        commentModel.setListCommentReply(listCommentReply);
        commentModels.add(commentModel);

        detailsNewsPresenter.commentModels = commentModels;

        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setId(12);
        newsDetail.setParent("21");
        detailsNewsPresenter.handleAddCommentIntoList(newsDetail);

    }

    @Test
    public void handleComplete_test(){
        detailsNewsPresenter.setDetailView(null);
        detailsNewsPresenter.handleComplete();
    }

    @Test
    public void handleComplete_testCompareDescent(){

        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(10);

        NewsDetail mainComment1 = new NewsDetail();
        mainComment1.setId(11);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);
        commentModels.add(commentModel);

        CommentModel commentModel1 = new CommentModel();
        commentModel1.setMainComment(mainComment1);
        commentModels.add(commentModel1);
        detailsNewsPresenter.commentModels = commentModels;

        detailsNewsPresenter.handleComplete();
    }

    @Test
    public void handleComplete_testCompareIncreare(){

        NewsDetail mainComment = new NewsDetail();
        mainComment.setId(11);

        NewsDetail mainComment1 = new NewsDetail();
        mainComment1.setId(12);

        List<CommentModel> commentModels = new ArrayList<>();
        CommentModel commentModel = new CommentModel();
        commentModel.setMainComment(mainComment);
        commentModels.add(commentModel);

        CommentModel commentModel1 = new CommentModel();
        commentModel1.setMainComment(mainComment1);
        commentModels.add(commentModel1);

        detailsNewsPresenter.commentModels = commentModels;

        detailsNewsPresenter.handleComplete();
    }

    @Test
    public void testGetApiStatus() {
        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);
        homeScreenPresenter.handleOnNextGetListNewContent(listCommentReply);
    }

    @Test
    public void testGetApiStatus_HomeView_Null() {
        homeScreenPresenter.setHomeView(null);
        List<NewsDetail> listCommentReply = new ArrayList<>();
        NewsDetail childComment = new NewsDetail();
        childComment.setId(11);
        childComment.setText("text");
        childComment.setBy("by");
        childComment.setTime(1234);
        listCommentReply.add(childComment);
        homeScreenPresenter.handleOnNextGetListNewContent(listCommentReply);
    }

    @Test
    public void testFragmentDetach() {
        DetailNewsFragment fragment = new DetailNewsFragment();
        fragment.onDetach();
        fragment.addCommentIntoUI(null);
    }

}
