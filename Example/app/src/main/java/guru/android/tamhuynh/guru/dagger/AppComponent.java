package guru.android.tamhuynh.guru.dagger;

import javax.inject.Singleton;

import dagger.Component;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailNewsFragment;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailsNewsPresenterImpl;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeActivity;
import guru.android.tamhuynh.guru.ui.homeScreen.HomeScreenPresenterImpl;

/**
 * Created by tamhuynh on 5/9/17.
 */
@Singleton
@Component(modules = {AppModule.class,PresenterModule.class, RestClientModule.class})
public interface AppComponent {
    void inject(HomeActivity target);

    void inject(DetailNewsFragment target);

    void inject(HomeScreenPresenterImpl target);

    void inject(DetailsNewsPresenterImpl target);
}
