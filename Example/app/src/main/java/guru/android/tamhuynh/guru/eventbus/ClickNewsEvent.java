package guru.android.tamhuynh.guru.eventbus;

import guru.android.tamhuynh.guru.model.NewsDetail;

/**
 * Created by Tam Huynh on 1/15/16.
 */
public class ClickNewsEvent {

    NewsDetail newsDetail;

    public ClickNewsEvent(NewsDetail newsDetail) {
        this.newsDetail = newsDetail;
    }

    public NewsDetail getNewsDetail() {
        return newsDetail;
    }

    public void setNewsDetail(NewsDetail newsDetail) {
        this.newsDetail = newsDetail;
    }
}
