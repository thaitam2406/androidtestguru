package guru.android.tamhuynh.guru;


import guru.android.tamhuynh.guru.dagger.AppComponent;
import guru.android.tamhuynh.guru.dagger.AppModule;
import guru.android.tamhuynh.guru.dagger.DaggerAppComponent;

/**
 * Created by tamhuynh on 5/9/17.
 */

public class MyApplication extends android.app.Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        /*if(Constant.IS_DEVLOPMENT){
            LeakCanary.install(this);
        }*/
        mAppComponent = initDagger(this);

    }

    private AppComponent initDagger (MyApplication application){
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public AppComponent getmAppComponent() {
        return mAppComponent;
    }
}
