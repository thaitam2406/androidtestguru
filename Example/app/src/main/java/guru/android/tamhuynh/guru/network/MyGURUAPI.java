package guru.android.tamhuynh.guru.network;


import java.util.List;

import guru.android.tamhuynh.guru.model.NewsDetail;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by tamhuynh on 1/31/16.
 */
public interface MyGURUAPI {

    @GET("topstories.json")
    Observable<List<String>> getHomeNewsList();

    @GET("item/{itemID}.json")
    Observable<NewsDetail> getDetailsNew(@Path("itemID") String itemID);

}
