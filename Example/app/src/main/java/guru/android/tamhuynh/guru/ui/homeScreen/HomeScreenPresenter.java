package guru.android.tamhuynh.guru.ui.homeScreen;

/**
 * Created by tamhuynh on 5/9/17.
 */

public interface HomeScreenPresenter {

    void getListNewIDs();

    void getListNewContent();

    void setHomeView(HomeView homeView);
}
