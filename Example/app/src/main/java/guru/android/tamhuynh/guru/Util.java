package guru.android.tamhuynh.guru;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by TamHuynh on 9/6/17.
 */

public class Util {

    //run multi-thread with limit as available core processors.
    public static Scheduler getScheduler() {
        // Get the max number of threads we could have
        int threadCount = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(threadCount);
        Scheduler scheduler = Schedulers.from(threadPoolExecutor);

        return scheduler;
    }

    public static String generateTime(long time1, long currentTime) {
//        long current = System.currentTimeMillis()/1000;
        long diff = currentTime - time1;
        int secondsTime = (int) diff;
        int seconds = secondsTime % 60;
        int minutes = secondsTime / (60) % 60;
        int hours = secondsTime / (60 * 60) % 24;
        int day = secondsTime / (24 * 60 * 60);
        int year = secondsTime / (24 * 60 * 60 * 365) % 365;

        if (year > 0) {
            return String.format("%d", year) + " years ago";
        } else if (day > 30) {
            return String.format("%d", day / 30) + " months ago";
        } else if (day > 0) {
            return String.format("%02d", day) + " days ago";
        }
        if (hours > 0) {
            return String.format("%02d hour %02d minute %02d second", hours, minutes, seconds) + " ago" ;
        }else{
            return String.format("%02d minute %02d second",minutes,seconds)+" ago";
        }
    }

    public static String getWebsiteName(String url){
        if(url == null || url.isEmpty())
            return "";
        String nameWebsite;
        String[] splitFlash = url.split("//");
        if(splitFlash.length > 1)
            nameWebsite = splitFlash[1].split("/")[0];
        else
            nameWebsite = splitFlash[0].split("/")[0];
        return nameWebsite;
    }

}
