package guru.android.tamhuynh.guru.ui.detailsScreen;

import java.util.List;

import guru.android.tamhuynh.guru.model.CommentModel;

/**
 * Created by tamhuynh on 5/9/17.
 */

public interface DetailNewsView {

    void addCommentIntoUI(List<CommentModel> commentModelList);
}
