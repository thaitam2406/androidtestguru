package guru.android.tamhuynh.guru.ui.homeScreen;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.rockerhieu.rvadapter.endless.EndlessRecyclerViewAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import guru.android.tamhuynh.guru.MyApplication;
import guru.android.tamhuynh.guru.R;
import guru.android.tamhuynh.guru.eventbus.ClickNewsEvent;
import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.ui.adapter.NewListAdapter;
import guru.android.tamhuynh.guru.ui.detailsScreen.DetailNewsFragment;

public class HomeActivity extends AppCompatActivity implements HomeView{

    @BindView(R.id.recycler_view_home)
    RecyclerView recyclerHomeView;
    @BindView(R.id.swipe_refresh)
    public SwipeRefreshLayout refreshLayoutHome;
    private NewListAdapter newListAdapter ;
    public EndlessRecyclerViewAdapter mEAdapter;
    @Inject
    HomeScreenPresenter homeScreenPresenter;
    private List<NewsDetail> newsDetails = new ArrayList<>();

    Unbinder unbinder ;
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((MyApplication) getApplication()).getmAppComponent().inject(this);
        unbinder = ButterKnife.bind(this);

        setUpRecyclerView();
        setUpPullToRefresh();
        //calling API
        homeScreenPresenter.setHomeView(this);
        homeScreenPresenter.getListNewIDs();

    }

    private void setUpPullToRefresh() {
        refreshLayoutHome.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handleOnRefreshData();
            }
        });
    }

    public void handleOnRefreshData() {
        if(newsDetails.size() > 0) {
            newsDetails.clear();
            newListAdapter.notifyDataSetChanged();
            homeScreenPresenter.getListNewIDs();
        }
        refreshLayoutHome.setRefreshing(false);
    }

    private void setUpRecyclerView() {
        //create adapter
        newListAdapter = new NewListAdapter(this,newsDetails);
        //Layout Manager
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerHomeView.setLayoutManager(llm);
        //Endless Adapter
        mEAdapter = new EndlessRecyclerViewAdapter(this, newListAdapter, new EndlessRecyclerViewAdapter.RequestToLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                handleOnLoadMoreRequest();
            }
        });
        //set into recyclerView
        recyclerHomeView.setAdapter(mEAdapter);
        newListAdapter.notifyDataSetChanged();
    }

    public void handleOnLoadMoreRequest() {
        if(newsDetails.size() > 0)
            homeScreenPresenter.getListNewContent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homeScreenPresenter.setHomeView(null);
        unbinder.unbind();
    }

    @Override
    public void addDataNewsIntoUI(List<NewsDetail> newsDetails) {
        if(newsDetails != null) {
            newListAdapter.addNewsList(newsDetails);
            newListAdapter.notifyDataSetChanged();
            mEAdapter.onDataReady(true);
//            mEAdapter.onBindViewHolder(null,newListAdapter.getItemCount());
        }else{
            mEAdapter.onDataReady(false);
        }
    }
    @Subscribe
    public void onEventMainThread(final ClickNewsEvent clickNewsEvent) {
        addFragmentComment(clickNewsEvent.getNewsDetail());
    }

    @Override
    public void onBackPressed() {
        int countFragment = getSupportFragmentManager().getBackStackEntryCount();
        if(countFragment > 0){
            getSupportFragmentManager().popBackStack();
        }else{
            finish();
        }
    }

    public void addFragmentComment(NewsDetail newsDetail){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        DetailNewsFragment detailNewsFragment = new DetailNewsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",newsDetail);
        detailNewsFragment.setArguments(bundle);

        ft.addToBackStack(DetailNewsFragment.class.getName());
        ft.add(R.id.container, detailNewsFragment,DetailNewsFragment.class.getName())
            .commitAllowingStateLoss();

    }

    public void setNewsDetails(List<NewsDetail> newsDetails) {
        this.newsDetails = newsDetails;
    }
}
