package guru.android.tamhuynh.guru.ui.homeScreen;

import java.util.List;

import guru.android.tamhuynh.guru.model.NewsDetail;

/**
 * Created by tamhuynh on 5/9/17.
 */

public interface HomeView {

    void addDataNewsIntoUI(List<NewsDetail> newsDetails);
}
