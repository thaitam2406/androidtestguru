package guru.android.tamhuynh.guru.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import guru.android.tamhuynh.guru.R;
import guru.android.tamhuynh.guru.Util;
import guru.android.tamhuynh.guru.eventbus.ClickNewsEvent;
import guru.android.tamhuynh.guru.model.NewsDetail;

/**
 * Created by TamHuynh on 9/6/17.
 */

public class NewListAdapter extends RecyclerView.Adapter<NewListAdapter.ViewHolderNewsList> {

    private  Context context;
    public List<NewsDetail> newsList;

    public NewListAdapter(Context context, List<NewsDetail> newsList) {
        this.context = context;
        this.newsList = newsList;
    }

    @Override
    public NewListAdapter.ViewHolderNewsList onCreateViewHolder(@Nullable ViewGroup parent,@Nullable int viewType) {
        View view = getLayout(parent);
        ViewHolderNewsList viewHolderNewsList = new ViewHolderNewsList(view);
        return viewHolderNewsList;
    }

    @Override
    public void onBindViewHolder(NewListAdapter.ViewHolderNewsList viewHolderNewsList, int position) {
        NewsDetail newsDetail = newsList.get(position);
        viewHolderNewsList.setNewsDetail(newsDetail);

        String title ;
        if(newsDetail.getTitle() == null){
            title = String.valueOf(position+1) + ". " ;
        }else{
            title = String.valueOf(position+1) + ". "  + newsDetail.getTitle();
        }
        String websiteName = Util.getWebsiteName(newsDetail.getUrl() == null ? "" : newsDetail.getUrl());
        viewHolderNewsList.tvTitle.setText(title + " (" + websiteName + ")");

        String numberOfKid = String.valueOf(newsDetail.getKids()!= null ? newsDetail.getKids().size() : 0);
        String core = String.valueOf(newsDetail.getScore() == null ? "" : newsDetail.getScore());
        String author  = newsDetail.getBy() == null ? "" : newsDetail.getBy();

        long currentTime = System.currentTimeMillis()/1000;
        String time = Util.generateTime(newsDetail.getTime() == null ? 0: newsDetail.getTime(), currentTime);
        String subTitle = core + " points by " + author + " " + time + " | " + numberOfKid + " comments";
        viewHolderNewsList.subTitle.setText(subTitle);

    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if(newsList != null)
            return newsList.size();
        return 0;
    }

    public static class ViewHolderNewsList extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tvTitle;
        private TextView subTitle;
        public View itemView;
        private NewsDetail newsDetail;

        public ViewHolderNewsList(View itemView) {
            super(itemView);
            this.itemView = itemView;
            itemView.setOnClickListener(this);
            tvTitle = (TextView) itemView.findViewById(R.id.title);
            subTitle = (TextView) itemView.findViewById(R.id.subtitle);
        }

        @Override
        public void onClick(View v) {
            tvTitle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new ClickNewsEvent(newsDetail));
                }
            },500);
        }

        public void setNewsDetail(NewsDetail newsDetail) {
            this.newsDetail = newsDetail;
        }
    }

    public void addNewsList(List<NewsDetail> newsDetails) {
        if(newsList == null)
            newsList = new ArrayList<>();
        this.newsList.addAll(newsDetails);
    }

    public View getLayout(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_home, null);
    }
}
