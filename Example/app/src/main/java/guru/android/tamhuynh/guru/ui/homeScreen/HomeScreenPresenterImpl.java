package guru.android.tamhuynh.guru.ui.homeScreen;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import guru.android.tamhuynh.guru.Constant;
import guru.android.tamhuynh.guru.MyApplication;
import guru.android.tamhuynh.guru.Util;
import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.network.MyGURUAPI;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tamhuynh on 5/9/17.
 */

public class HomeScreenPresenterImpl implements HomeScreenPresenter {

    HomeView homeView;
    @Inject
    MyGURUAPI restClientModule;
    public int currentPosition = 0;
    public int total = 0;
    public List<String> listNewsID;

    public HomeScreenPresenterImpl(Context context) {
        ((MyApplication) context).getmAppComponent().inject(this);
    }

    @Override
    public void getListNewIDs() {
        currentPosition = 0;
        restClientModule.getHomeNewsList()
                .subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Observer<List<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<String> responseServer) {
                        if(responseServer != null) {
                            total = responseServer.size();
                            listNewsID = responseServer;
                            getListNewContent();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void getListNewContent() {
        if(currentPosition < total) {
            List<Observable<NewsDetail>> observableList = new ArrayList<>();
            int maxPos = currentPosition + Constant.PAGING;
            if(maxPos > listNewsID.size())
                maxPos = listNewsID.size();
            for (int i = currentPosition; i < maxPos; i++) {
                String id = listNewsID.get(i);
                if (id != null && !id.isEmpty()) {
                    Observable<NewsDetail> observable = restClientModule.getDetailsNew(id);
                    observableList.add(observable);
                }
            }
            currentPosition += Constant.PAGING;
            Observable/*.concat(observableList).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<NewsDetail>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(NewsDetail newsDetail) {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                            Logger.d("complete");
                            homeView.addDataNewsIntoUI(null);
                        }
                    });*/
            .zip(observableList, new Function<Object[], List<NewsDetail>>() {
                @Override
                public List<NewsDetail> apply(@NonNull Object[] objects) throws Exception {
                    List<NewsDetail> newsDetails = new ArrayList<>();
                    for (Object a : objects) {
                        NewsDetail newsDetail = (NewsDetail) a;
                        newsDetails.add(newsDetail);
                    }
                    return newsDetails;
                }
            }).subscribeOn(Util.getScheduler())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<NewsDetail>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<NewsDetail> newsDetails) {
                            handleOnNextGetListNewContent(newsDetails);
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }else{
            if(homeView != null)
                homeView.addDataNewsIntoUI(null);
        }
    }

    public void handleOnNextGetListNewContent(List<NewsDetail> newsDetails) {
        if(homeView != null)
            homeView.addDataNewsIntoUI(newsDetails);
    }

    @Override
    public void setHomeView(HomeView homeView) {
        this.homeView = homeView;
    }
}
