package guru.android.tamhuynh.guru.ui.detailsScreen;

/**
 * Created by tamhuynh on 5/9/17.
 */

public interface DetailNewsPresenter {

    void setDetailView(DetailNewsView detailView);

    void getListComment(String newID);

    void resetData();
}
