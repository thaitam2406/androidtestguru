package guru.android.tamhuynh.guru.ui.detailsScreen;

import android.content.Context;

import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import guru.android.tamhuynh.guru.MyApplication;
import guru.android.tamhuynh.guru.model.CommentModel;
import guru.android.tamhuynh.guru.model.NewsDetail;
import guru.android.tamhuynh.guru.network.MyGURUAPI;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.R.attr.id;

/**
 * Created by tamhuynh on 5/9/17.
 */

public class DetailsNewsPresenterImpl implements DetailNewsPresenter {

    DetailNewsView detailNewsView;
    @Inject
    MyGURUAPI myGURUAPI;

    public List<CommentModel> commentModels = new ArrayList<>();
    int counter;
    int total = 1;

    public DetailsNewsPresenterImpl(Context context) {
        ((MyApplication) context).getmAppComponent().inject(this);
    }

    @Override
    public void setDetailView(DetailNewsView detailView) {
        this.detailNewsView = detailView;
    }

    @Override
    public void getListComment(String newID) {
        myGURUAPI.getDetailsNew(newID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewsDetail>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Logger.d("complete");
                    }

                    @Override
                    public void onNext(NewsDetail newsDetail) {
                        if (newsDetail != null) {
                            if (newsDetail.getType().equals("comment"))
                                handleAddCommentIntoList(newsDetail);
                            List<Integer> lstCommentChild = newsDetail.getKids();
                            if (lstCommentChild != null) {
                                for (Integer id : lstCommentChild) {
                                    String idNew = id.toString();
                                    getListComment(idNew);
                                    total += 1;
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.d(e.getMessage());
                        handleComplete();
                    }

                    @Override
                    public void onComplete() {
                        handleComplete();
                    }
                });
    }

    public void handleComplete() {
        counter += 1;
        if (counter >= total) {
            Collections.sort(commentModels, new Comparator<CommentModel>() {
                @Override
                public int compare(CommentModel obj1, CommentModel obj2) {
                    return obj2.getMainComment().getId().toString().compareToIgnoreCase(
                            obj1.getMainComment().getId().toString());
                }
            });
            if (detailNewsView != null)
                detailNewsView.addCommentIntoUI(commentModels);
        }
    }

    @Override
    public void resetData() {
        commentModels.clear();
        commentModels = new ArrayList<>();
        counter = 0;
        total = 1;
    }

    public void handleAddCommentIntoList(NewsDetail newsDetail) {
        String parentID = newsDetail.getParent();
        if (commentModels != null) {
            if (commentModels.size() > 0) {
                for (CommentModel commentModel : commentModels) {
                    /**
                     * Check parentID with Main Comment
                     */
                    if (commentModel != null) {
                        if (commentModel.getMainComment() != null) {
                            String mainCommentID = commentModel.getMainComment().getId().toString();
                            if (parentID.equals(mainCommentID)) {
                                if (commentModel.getListCommentReply() == null)
                                    commentModel.setListCommentReply(new ArrayList<NewsDetail>());
                                commentModel.getListCommentReply().add(newsDetail);
                                return;
                            }
                        }


                        /**
                         * Check parentID with Child Comments
                         */
                        List<NewsDetail> newsDetails = commentModel.getListCommentReply();
                        if (newsDetails != null) {
                            for (NewsDetail childComment : newsDetails) {
                                if (childComment != null) {
                                    String id = childComment.getId().toString();
                                    if (id.equals(parentID)) {
                                        commentModel.getListCommentReply().add(newsDetail);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /**
             * add new comment model if the parentID of this not match with any existing one
             */
            CommentModel commentModel = new CommentModel();
            commentModel.setMainComment(newsDetail);
            commentModels.add(commentModel);
        }
    }
}
