package guru.android.tamhuynh.guru.ui.detailsScreen;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import guru.android.tamhuynh.guru.MyApplication;
import guru.android.tamhuynh.guru.R;
import guru.android.tamhuynh.guru.Util;
import guru.android.tamhuynh.guru.model.CommentModel;
import guru.android.tamhuynh.guru.model.NewsDetail;

/**
 * Created by tamhuynh on 5/9/17.
 */

public class DetailNewsFragment extends Fragment implements DetailNewsView {

    @Inject
    DetailNewsPresenter detailNewsPresenter;
    @BindView(R.id.layout_comment)
    LinearLayout layoutComment;
    @BindView(R.id.layout_progress_bar)
    RelativeLayout layoutProgressBar;

    Unbinder unbinder;
    boolean isDetach = false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApplication) getContext().getApplicationContext()).getmAppComponent().inject(DetailNewsFragment.this);
        detailNewsPresenter.setDetailView(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_comment_news,container,false);
        unbinder = ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null ){
            if(bundle.get("data") != null) {
                NewsDetail newsDetail = (NewsDetail) bundle.getSerializable("data");
                String idComment = String.valueOf(newsDetail.getId());
                detailNewsPresenter.resetData();
                detailNewsPresenter.getListComment(idComment);
                layoutProgressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void addCommentIntoUI(List<CommentModel> commentModelList) {
        if(!isDetach) {
            layoutProgressBar.setVisibility(View.GONE);
            if (commentModelList != null) {
                for (CommentModel commentModel : commentModelList) {
                    if (commentModel != null) {
                        if (commentModel.getMainComment() != null) {
                            handleMainComment(commentModel.getMainComment(), commentModel);
                            if (commentModel.getListCommentReply() != null) {
                                for (NewsDetail newsDetail : commentModel.getListCommentReply()) {
                                    handleChildComment(newsDetail);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle UI childs comment
     * @param newsDetail
     */
    private void handleChildComment(NewsDetail newsDetail) {
        TextView childTVAuthor = getTextView();
        childTVAuthor.setPadding(100,5,5,5);
        childTVAuthor.setTypeface(Typeface.DEFAULT_BOLD);
        setAuthorComment(childTVAuthor,newsDetail);
        childTVAuthor.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_comment_black_18dp, 0, 0, 0);
        layoutComment.addView(childTVAuthor);

        TextView childTV = getTextView();
        childTV.setPadding(100,10,10,10);
        setTextComment(childTV,newsDetail);
        layoutComment.addView(childTV);
    }

    /**
     * Hnalde UI Main Comment
     * @param mainCommentDetail
     * @param commentModel
     */
    private void handleMainComment(NewsDetail mainCommentDetail, CommentModel commentModel) {
        TextView mainCommentAuthor = getTextView();
        mainCommentAuthor.setPadding(40,30,5,5);
        mainCommentAuthor.setTypeface(Typeface.DEFAULT_BOLD);
        setAuthorComment(mainCommentAuthor,mainCommentDetail);
        mainCommentAuthor.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_comment_black_18dp, 0, 0, 0);
        layoutComment.addView(mainCommentAuthor);

        TextView mainComment = getTextView();
        mainComment.setPadding(40,10,10,10);
        setTextComment(mainComment,mainCommentDetail);
        layoutComment.addView(mainComment);

        TextView replyTV = getTextView();
        replyTV.setPadding(100,10,10,10);
        replyTV.setTypeface(Typeface.SERIF);
        replyTV.setPaintFlags(replyTV.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        setReplyText(replyTV, commentModel);
        layoutComment.addView(replyTV);
    }

    public TextView getTextView(){
        TextView tv = new TextView(getContext());
        return tv;
    }

    public void setAuthorComment(TextView tv, NewsDetail newsDetail){
        String by =  newsDetail.getBy() == null ? "" : newsDetail.getBy();
        long time = newsDetail.getTime() == null ? 0 : newsDetail.getTime();
        long currentTime = System.currentTimeMillis()/1000;
        tv.setText(by + " " + Util.generateTime(time, currentTime));
    }

    public void setTextComment(TextView tv, NewsDetail newsDetail){
        String formattedText = newsDetail.getText() == null ? "" : newsDetail.getText();
        Spanned result = Html.fromHtml(formattedText);
        tv.setText(result);
    }

    public void setReplyText(TextView tv, CommentModel commentModel){
        int commentSize ;
        if(commentModel.getListCommentReply() != null)
            commentSize = commentModel.getListCommentReply().size();
        else{
            commentSize = 0;
        }
        String sizeComments = String.valueOf(commentSize);
        tv.setText("Reply" + "(" + sizeComments + ")" );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        isDetach = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
