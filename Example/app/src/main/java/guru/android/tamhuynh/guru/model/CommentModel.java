package guru.android.tamhuynh.guru.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tamhuynh on 6/9/17.
 */

/**
 * Composite model, if one comment has any reply, they will be hold in (listCommentReply)
 */
public class CommentModel {

    private NewsDetail mainComment;

    private List<NewsDetail> listCommentReply;


    public NewsDetail getMainComment() {
        return mainComment;
    }

    public void setMainComment(NewsDetail mainComment) {
        this.mainComment = mainComment;
    }

    public List<NewsDetail> getListCommentReply() {
        return listCommentReply ;
    }

    public void setListCommentReply(List<NewsDetail> listCommentReply) {
        this.listCommentReply = listCommentReply;
    }
}
