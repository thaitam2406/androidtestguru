package guru.android.tamhuynh.guru;

/**
 * Created by tamhuynh on 5/9/17.
 */

public class Constant {

    public static boolean IS_DEVLOPMENT = true;
    public static String BASE_URL = "https://hacker-news.firebaseio.com/v0/";
    public static int PAGING = 25;
}
